package com.adasoraninda.cifproject.controller;

import com.adasoraninda.cifproject.controller.endpoint.MCifEndPoint;
import com.adasoraninda.cifproject.model.request.MCifRequest;
import com.adasoraninda.cifproject.model.response.BaseResponse;
import com.adasoraninda.cifproject.model.response.MCifResponse;
import com.adasoraninda.cifproject.service.MCifService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.adasoraninda.cifproject.controller.AppRequestUtil.DEFAULT_ROLE;
import static com.adasoraninda.cifproject.controller.AppRequestUtil.ROLE_PARAM;

@RestController
@AllArgsConstructor
@RequestMapping(path = MCifEndPoint.pathBase)
public class MCifController {

    private final MCifService service;

    @GetMapping
    public BaseResponse<List<MCifResponse>> getListCIF() {
        var response = service.getListCif();

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @GetMapping(path = MCifEndPoint.pathId)
    public BaseResponse<MCifResponse> getCIFById(
            @PathVariable(value = MCifEndPoint.variableId) Long cifId
    ) {
        var response = service.getCifById(cifId);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BaseResponse<MCifResponse> createCIF(
            @Valid @RequestBody MCifRequest cifRequest,
            @RequestParam(name = ROLE_PARAM, defaultValue = DEFAULT_ROLE, required = false) String role
    ) {
        var response = service.createCif(cifRequest, role);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @PutMapping(path = MCifEndPoint.pathId)
    public BaseResponse<MCifResponse> updateCIF(
            @PathVariable(value = MCifEndPoint.variableId) Long cifId,
            @RequestParam(name = ROLE_PARAM, defaultValue = DEFAULT_ROLE, required = false) String role,
            @Valid @RequestBody MCifRequest cifRequest) {
        var response = service.updateCif(cifId, role, cifRequest);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @DeleteMapping(path = MCifEndPoint.pathId)
    public BaseResponse<Object> deleteCIFById(
            @PathVariable(value = MCifEndPoint.variableId) Long cifId,
            @RequestParam(name = ROLE_PARAM, defaultValue = DEFAULT_ROLE, required = false) String role
    ) {
        var response = service.deleteCifById(cifId, role);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

}
