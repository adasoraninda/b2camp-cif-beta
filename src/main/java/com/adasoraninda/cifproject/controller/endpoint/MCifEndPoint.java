package com.adasoraninda.cifproject.controller.endpoint;

public final class MCifEndPoint {
    public static final String variableId = "id";
    public static final String pathBase = "cif";
    public static final String pathId = "/{" + variableId + "}";
}
