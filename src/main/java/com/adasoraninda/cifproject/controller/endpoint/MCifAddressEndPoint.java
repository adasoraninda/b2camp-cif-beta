package com.adasoraninda.cifproject.controller.endpoint;

public final class MCifAddressEndPoint {
    public static final String variableId = "id";
    public static final String pathBase = "addresses";
    public static final String pathId = "/{" + variableId + "}";
}
