package com.adasoraninda.cifproject.repository;

import com.adasoraninda.cifproject.model.entity.MCifWork;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface MCifWorkRepository extends JpaRepository<MCifWork, Long> {

    @Query(value = "SELECT * FROM m_cif_work " +
            "WHERE is_deleted = false",
            nativeQuery = true)
    List<MCifWork> findAllCifWork();

    @Query(value = "SELECT * FROM m_cif_work " +
            "WHERE is_deleted = false " +
            "AND id = ?1",
            nativeQuery = true)
    Optional<MCifWork> findCifWorkById(Long id);

    @Query(value = "SELECT COUNT(*) > 0 " +
            "FROM m_cif_work " +
            "WHERE id = ?1",
            nativeQuery = true)
    boolean existCifWorkById(Long id);

    @Query(value = "SELECT COUNT(*) FROM m_cif_work " +
            "WHERE cif_id = ?1 ",
            nativeQuery = true)
    int getTotalWorkByCifId(Long id);

    @Modifying
    @Query(value = "UPDATE m_cif_work " +
            "SET updated_by = ?2, " +
            "updated_date = ?3, " +
            "is_deleted = true " +
            "WHERE id = ?1",
            nativeQuery = true)
    void softDelete(Long id, String role, LocalDateTime dateTime);

}
