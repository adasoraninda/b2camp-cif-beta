package com.adasoraninda.cifproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CifProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(CifProjectApplication.class, args);
    }

}
