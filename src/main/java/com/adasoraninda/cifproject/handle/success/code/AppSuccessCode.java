package com.adasoraninda.cifproject.handle.success.code;

public enum AppSuccessCode {
    GET_ALL_DATA, GET_DATA, CREATE_DATA, UPDATE_DATA, DELETE_DATA;
}
