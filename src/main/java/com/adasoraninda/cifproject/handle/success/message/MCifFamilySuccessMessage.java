package com.adasoraninda.cifproject.handle.success.message;

import com.adasoraninda.cifproject.handle.AppMessage;
import com.adasoraninda.cifproject.handle.exception.AppCodeException;
import com.adasoraninda.cifproject.handle.success.code.AppSuccessCode;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class MCifFamilySuccessMessage implements AppMessage {

    private final AppSuccessCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case GET_ALL_DATA:
                return successGetFamilies();
            case GET_DATA:
                return successGetFamily();
            case CREATE_DATA:
                return successCreateFamily();
            case UPDATE_DATA:
                return successUpdateFamily();
            case DELETE_DATA:
                return successDeleteFamily();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String successGetFamilies() {
        return "Berhasil mendapatkan semua data keluarga";
    }

    private String successGetFamily() {
        return "Berhasil mendapatkan data keluarga";
    }

    private String successCreateFamily() {
        return "Berhasil menambahkan anggota keluarga";
    }

    private String successUpdateFamily() {
        return "Berhasil mengubah anggota keluarga";
    }

    private String successDeleteFamily() {
        return "Berhasil menghapus anggota keluarga";
    }

}
