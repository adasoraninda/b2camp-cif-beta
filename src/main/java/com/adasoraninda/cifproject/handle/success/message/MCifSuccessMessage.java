package com.adasoraninda.cifproject.handle.success.message;

import com.adasoraninda.cifproject.handle.AppMessage;
import com.adasoraninda.cifproject.handle.exception.AppCodeException;
import com.adasoraninda.cifproject.handle.success.code.AppSuccessCode;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class MCifSuccessMessage implements AppMessage {

    private final AppSuccessCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case GET_ALL_DATA:
                return successGetAllCif();
            case GET_DATA:
                return successGetCif();
            case CREATE_DATA:
                return successCreateCif();
            case UPDATE_DATA:
                return successUpdateCif();
            case DELETE_DATA:
                return successDeleteCif();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String successGetAllCif() {
        return "Berhasil mendapatkan semua data cif";
    }

    private String successGetCif() {
        return "Berhasil mendapatkan data cif";
    }

    private String successCreateCif() {
        return "Berhasil menambahkan data cif";
    }

    private String successUpdateCif() {
        return "Berhasil mengubah data cif";
    }

    private String successDeleteCif() {
        return "Berhasil menghapus data cif";
    }

}
