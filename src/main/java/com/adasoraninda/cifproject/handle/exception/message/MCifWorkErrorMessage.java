package com.adasoraninda.cifproject.handle.exception.message;

import com.adasoraninda.cifproject.handle.AppMessage;
import com.adasoraninda.cifproject.handle.exception.AppCodeException;
import com.adasoraninda.cifproject.handle.exception.code.MCifWorkErrorCode;
import lombok.AllArgsConstructor;

import java.util.Map;

import static com.adasoraninda.cifproject.handle.exception.code.MCifWorkErrorCode.*;

@AllArgsConstructor
public final class MCifWorkErrorMessage implements AppMessage {

    private final Map<MCifWorkErrorCode, Object> data;
    private final MCifWorkErrorCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case CIF_WORK_NOT_FOUND:
                return errorNotFoundWithId();
            case CIF_WORK_ALREADY_EXISTS:
                return errorAlreadyExistsWithId();
            case CIF_WORK_IS_EMPTY:
                return errorIsEmpty();
            case CIF_WORK_MAX:
                return errorLimit();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String errorNotFoundWithId() {
        return "Data pekerjaan dengan id " + data.get(CIF_WORK_NOT_FOUND) + " tidak ditemukan";
    }

    private String errorAlreadyExistsWithId() {
        return "Data pekerjaan dengan id " + data.get(CIF_WORK_ALREADY_EXISTS) + " sudah ada";
    }

    private String errorIsEmpty() {
        return "Data pekerjaan tidak ada";
    }

    private String errorLimit() {
        return "Pekerjaan hanya diperbolehkan sebanyak " + data.get(CIF_WORK_MAX) + " pekerjaan";
    }

}
