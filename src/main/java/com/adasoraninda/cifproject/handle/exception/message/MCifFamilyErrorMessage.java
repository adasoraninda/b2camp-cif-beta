package com.adasoraninda.cifproject.handle.exception.message;

import com.adasoraninda.cifproject.handle.AppMessage;
import com.adasoraninda.cifproject.handle.exception.AppCodeException;
import com.adasoraninda.cifproject.handle.exception.code.MCifFamilyErrorCode;
import lombok.AllArgsConstructor;

import java.util.Map;

import static com.adasoraninda.cifproject.handle.exception.code.MCifFamilyErrorCode.CIF_FAMILY_ALREADY_EXISTS;
import static com.adasoraninda.cifproject.handle.exception.code.MCifFamilyErrorCode.CIF_FAMILY_NOT_FOUND;

@AllArgsConstructor
public class MCifFamilyErrorMessage implements AppMessage {

    private final Map<MCifFamilyErrorCode, Object> data;
    private final MCifFamilyErrorCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case CIF_FAMILY_NOT_FOUND:
                return errorNotFoundWithId();
            case CIF_FAMILY_ALREADY_EXISTS:
                return errorAlreadyExistsWithId();
            case CIF_FAMILY_IS_EMPTY:
                return errorIsEmpty();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String errorNotFoundWithId() {
        return "Data keluarga dengan id " + data.get(CIF_FAMILY_NOT_FOUND) + " tidak ditemukan";
    }

    private String errorAlreadyExistsWithId() {
        return "Data keluarga " + data.get(CIF_FAMILY_ALREADY_EXISTS) + " sudah ada";
    }

    private String errorIsEmpty() {
        return "Data keluarga tidak ada";
    }

}