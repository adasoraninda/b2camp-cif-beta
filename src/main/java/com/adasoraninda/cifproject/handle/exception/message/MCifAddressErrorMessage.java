package com.adasoraninda.cifproject.handle.exception.message;

import com.adasoraninda.cifproject.handle.AppMessage;
import com.adasoraninda.cifproject.handle.exception.AppCodeException;
import com.adasoraninda.cifproject.handle.exception.code.MCifAddressErrorCode;
import lombok.AllArgsConstructor;

import java.util.Map;

import static com.adasoraninda.cifproject.handle.exception.code.MCifAddressErrorCode.CIF_ADDRESS_ALREADY_EXISTS;
import static com.adasoraninda.cifproject.handle.exception.code.MCifAddressErrorCode.CIF_ADDRESS_NOT_FOUND;

@AllArgsConstructor
public class MCifAddressErrorMessage implements AppMessage {

    private final Map<MCifAddressErrorCode, Object> data;
    private final MCifAddressErrorCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case CIF_ADDRESS_NOT_FOUND:
                return errorNotFoundWithId();
            case CIF_ADDRESS_ALREADY_EXISTS:
                return errorAlreadyExistsWithId();
            case CIF_ADDRESS_IS_EMPTY:
                return errorIsEmpty();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String errorNotFoundWithId() {
        return "Data alamat dengan id " + data.get(CIF_ADDRESS_NOT_FOUND) + " tidak ditemukan";
    }

    private String errorAlreadyExistsWithId() {
        return "Data alamat denga id " + data.get(CIF_ADDRESS_ALREADY_EXISTS) + " sudah ada";
    }

    private String errorIsEmpty() {
        return "Data alamat tidak ada";
    }

}
