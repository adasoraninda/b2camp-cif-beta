package com.adasoraninda.cifproject.handle.exception.message;

import com.adasoraninda.cifproject.handle.AppMessage;
import com.adasoraninda.cifproject.handle.exception.AppCodeException;
import com.adasoraninda.cifproject.handle.exception.code.MCifErrorCode;
import lombok.AllArgsConstructor;

import java.util.Map;

import static com.adasoraninda.cifproject.handle.exception.code.MCifErrorCode.CIF_ALREADY_EXISTS;
import static com.adasoraninda.cifproject.handle.exception.code.MCifErrorCode.CIF_NOT_FOUND;

@AllArgsConstructor
public class MCifErrorMessage implements AppMessage {

    private final Map<MCifErrorCode, Object> data;
    private final MCifErrorCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case CIF_NOT_FOUND:
                return errorNotFoundWithId();
            case CIF_ALREADY_EXISTS:
                return errorAlreadyExistsWithId();
            case CIF_IS_EMPTY:
                return errorIsEmpty();
            case CIF_NPWP_MUST_FILLED:
                return errorNpwpMustBeFilled();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String errorNotFoundWithId() {
        return "CIF dengan id " + data.get(CIF_NOT_FOUND) + " tidak ditemukan";
    }

    private String errorAlreadyExistsWithId() {
        return "CIF dengan id " + data.get(CIF_ALREADY_EXISTS) + " sudah ada";
    }

    private String errorIsEmpty() {
        return "CIF tidak ada";
    }

    private String errorNpwpMustBeFilled() {
        return "NPWP dengan tipe COMPANY harus di isi";
    }

}
