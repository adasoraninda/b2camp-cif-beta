package com.adasoraninda.cifproject.handle.exception.message;

import com.adasoraninda.cifproject.handle.exception.AppCodeException;

public class SqlErrorMessage {

    public static String getMessage(String key) {
        if (key.equalsIgnoreCase("id_ktp")) {
            return "KTP sudah terdaftar";
        } else if (key.equalsIgnoreCase("npwp")) {
            return "NPWP sudah terdaftar";
        } else if (key.equalsIgnoreCase("no_telephone")) {
            return "No telepon sudah terdaftar";
        } else if (key.equalsIgnoreCase("email")) {
            return "Email sudah terdaftar";
        }

        throw new AppCodeException(key);
    }

}
