package com.adasoraninda.cifproject.handle.exception.code;

public enum MCifWorkErrorCode {
    CIF_WORK_NOT_FOUND,
    CIF_WORK_ALREADY_EXISTS,
    CIF_WORK_IS_EMPTY,
    CIF_WORK_MAX;
}
