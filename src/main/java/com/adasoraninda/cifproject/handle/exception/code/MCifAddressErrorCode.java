package com.adasoraninda.cifproject.handle.exception.code;

public enum MCifAddressErrorCode {
    CIF_ADDRESS_NOT_FOUND,
    CIF_ADDRESS_ALREADY_EXISTS,
    CIF_ADDRESS_IS_EMPTY;
}
