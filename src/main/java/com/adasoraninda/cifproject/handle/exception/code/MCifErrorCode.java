package com.adasoraninda.cifproject.handle.exception.code;

public enum MCifErrorCode {
    CIF_NOT_FOUND,
    CIF_ALREADY_EXISTS,
    CIF_IS_EMPTY,
    CIF_NPWP_MUST_FILLED;
}
