package com.adasoraninda.cifproject.handle.exception.code;

public enum MCifFamilyErrorCode {
    CIF_FAMILY_NOT_FOUND,
    CIF_FAMILY_ALREADY_EXISTS,
    CIF_FAMILY_IS_EMPTY;
}
