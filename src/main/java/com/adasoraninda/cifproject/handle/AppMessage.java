package com.adasoraninda.cifproject.handle;

public interface AppMessage {
    String getCode();
    String getMessage();
}
