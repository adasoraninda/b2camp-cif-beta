package com.adasoraninda.cifproject.handle;

import com.adasoraninda.cifproject.handle.exception.BusinessException;
import com.adasoraninda.cifproject.handle.exception.message.SqlErrorMessage;
import com.adasoraninda.cifproject.model.response.BaseResponse;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.util.stream.Collectors;

@RestControllerAdvice
public class AppExceptionHandler {

    @ExceptionHandler(value = {Exception.class, Error.class})
    public ResponseEntity<Object> handleAllError(Throwable ex) {
        var message = ex.getCause().getCause().getLocalizedMessage();

        if (message.contains("Detail:")) {
            var key = message.substring(message.indexOf("(") + 1, message.indexOf(")"));

            try {
                message = SqlErrorMessage.getMessage(key);
            } catch (Exception ignored) {
            }

        }

        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(BaseResponse.error(message));
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    public ResponseEntity<Object> handlePathError(NoHandlerFoundException ex) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(BaseResponse.error(ex.getLocalizedMessage()));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleInputError(MethodArgumentNotValidException ex) {
        var message = ex.getBindingResult().getAllErrors().stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.joining(", "));

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(BaseResponse.error(message));
    }

    @ExceptionHandler(value = {BusinessException.class})
    public ResponseEntity<Object> handleAppError(Exception ex) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(BaseResponse.error(
                        ex.toString(),
                        ex.getLocalizedMessage()));
    }

}
