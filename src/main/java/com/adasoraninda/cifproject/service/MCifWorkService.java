package com.adasoraninda.cifproject.service;

import com.adasoraninda.cifproject.handle.AppSuccessHandler;
import com.adasoraninda.cifproject.model.request.MCifWorkRequest;
import com.adasoraninda.cifproject.model.response.MCifWorkResponse;

import java.util.List;

public interface MCifWorkService {

    AppSuccessHandler<List<MCifWorkResponse>> getWorks();

    AppSuccessHandler<MCifWorkResponse> getWorkById(Long workId);

    AppSuccessHandler<MCifWorkResponse> createWork(MCifWorkRequest workRequest, String role);

    AppSuccessHandler<MCifWorkResponse> updateWork(Long workId, String role, MCifWorkRequest workRequest);

    AppSuccessHandler<Object> deleteWorkById(Long workId, String role);

}
