package com.adasoraninda.cifproject.service.impl;

import com.adasoraninda.cifproject.handle.AppSuccessHandler;
import com.adasoraninda.cifproject.handle.exception.BusinessException;
import com.adasoraninda.cifproject.handle.exception.message.MCifErrorMessage;
import com.adasoraninda.cifproject.handle.success.code.AppSuccessCode;
import com.adasoraninda.cifproject.handle.success.message.MCifSuccessMessage;
import com.adasoraninda.cifproject.model.entity.MCif;
import com.adasoraninda.cifproject.model.request.MCifRequest;
import com.adasoraninda.cifproject.model.response.MCifResponse;
import com.adasoraninda.cifproject.repository.MCifRepository;
import com.adasoraninda.cifproject.service.MCifService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.adasoraninda.cifproject.controller.AppRequestUtil.COMPANY_CIF_TYPE;
import static com.adasoraninda.cifproject.handle.exception.code.MCifErrorCode.*;

@Service
@AllArgsConstructor
public class MCifServiceImpl implements MCifService {

    private final ModelMapper modelMapper;
    private final MCifRepository cifRepository;

    @Override
    public AppSuccessHandler<List<MCifResponse>> getListCif() {
        var listCif = cifRepository.findAllCif();

        if (listCif.isEmpty()) {
            throw throwCifIsEmpty();
        }

        var message = new MCifSuccessMessage(AppSuccessCode.GET_ALL_DATA);
        var allCifResponse = listCif.stream()
                .map(cif -> modelMapper.map(cif, MCifResponse.class))
                .collect(Collectors.toList());

        return new AppSuccessHandler<>(message, allCifResponse);
    }

    @Override
    public AppSuccessHandler<MCifResponse> getCifById(Long cifId) {
        var cifResponse = cifRepository.findCifById(cifId)
                .map(cif -> modelMapper.map(cif, MCifResponse.class))
                .orElseThrow(() -> throwCifNotFound(cifId));
        var message = new MCifSuccessMessage(AppSuccessCode.GET_DATA);

        return new AppSuccessHandler<>(message, cifResponse);
    }

    @Override
    @Transactional
    public AppSuccessHandler<MCifResponse> createCif(MCifRequest cifRequest, String role) {
        var cif = modelMapper.map(cifRequest, MCif.class);

        if (cif.getType().equalsIgnoreCase(COMPANY_CIF_TYPE)) {
            if (cif.getNpwp() == null) {
                throw throwCifNpwp();
            }
        }

        cif.create(role);

        var message = new MCifSuccessMessage(AppSuccessCode.CREATE_DATA);
        var cifResponse = modelMapper.map(
                cifRepository.save(cif),
                MCifResponse.class);

        return new AppSuccessHandler<>(message, cifResponse);
    }

    @Override
    @Transactional
    public AppSuccessHandler<MCifResponse> updateCif(Long cifId, String role, MCifRequest cifRequest) {
        var oldCif = cifRepository.findCifById(cifId)
                .orElseThrow(() -> throwCifNotFound(cifId));

        oldCif.update(modelMapper.map(cifRequest, MCif.class), role);

        var message = new MCifSuccessMessage(AppSuccessCode.UPDATE_DATA);
        var cifResponse = modelMapper.map(
                cifRepository.save(oldCif),
                MCifResponse.class);

        return new AppSuccessHandler<>(message, cifResponse);
    }

    @Override
    @Transactional
    public AppSuccessHandler<Object> deleteCifById(Long cifId, String role) {
        var cifExists = cifRepository.existCifById(cifId);

        if (!cifExists) {
            throw throwCifNotFound(cifId);
        }

        cifRepository.softDelete(cifId, role, LocalDateTime.now());

        var message = new MCifSuccessMessage(AppSuccessCode.DELETE_DATA);

        return new AppSuccessHandler<>(message, null);
    }

    private RuntimeException throwCifIsEmpty() {
        return new BusinessException(
                new MCifErrorMessage(
                        Map.of(CIF_IS_EMPTY, new Object()),
                        CIF_IS_EMPTY));
    }

    private RuntimeException throwCifNotFound(Long cifId) {
        return new BusinessException(
                new MCifErrorMessage(
                        Map.of(CIF_NOT_FOUND, cifId),
                        CIF_NOT_FOUND));
    }

    private RuntimeException throwCifNpwp() {
        return new BusinessException(
                new MCifErrorMessage(
                        Map.of(CIF_NPWP_MUST_FILLED, new Object()),
                        CIF_NPWP_MUST_FILLED));
    }

}
