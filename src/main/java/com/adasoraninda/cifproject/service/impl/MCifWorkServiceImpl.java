package com.adasoraninda.cifproject.service.impl;

import com.adasoraninda.cifproject.handle.AppSuccessHandler;
import com.adasoraninda.cifproject.handle.exception.BusinessException;
import com.adasoraninda.cifproject.handle.exception.message.MCifErrorMessage;
import com.adasoraninda.cifproject.handle.exception.message.MCifWorkErrorMessage;
import com.adasoraninda.cifproject.handle.success.code.AppSuccessCode;
import com.adasoraninda.cifproject.handle.success.message.MCifWorkSuccessMessage;
import com.adasoraninda.cifproject.model.entity.MCifWork;
import com.adasoraninda.cifproject.model.request.MCifWorkRequest;
import com.adasoraninda.cifproject.model.response.MCifWorkResponse;
import com.adasoraninda.cifproject.repository.MCifRepository;
import com.adasoraninda.cifproject.repository.MCifWorkRepository;
import com.adasoraninda.cifproject.service.MCifWorkService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.adasoraninda.cifproject.controller.AppRequestUtil.MAX_WORK;
import static com.adasoraninda.cifproject.handle.exception.code.MCifErrorCode.CIF_NOT_FOUND;
import static com.adasoraninda.cifproject.handle.exception.code.MCifWorkErrorCode.*;

@Service
@AllArgsConstructor
public class MCifWorkServiceImpl implements MCifWorkService {
    private final ModelMapper modelMapper;

    private final MCifRepository cifRepository;
    private final MCifWorkRepository workRepository;

    @Override
    public AppSuccessHandler<List<MCifWorkResponse>> getWorks() {
        var works = workRepository.findAllCifWork();

        if (works.isEmpty()) {
            throw throwEmptyWorks();
        }

        var message = new MCifWorkSuccessMessage(AppSuccessCode.GET_ALL_DATA);
        var worksResponse = works.stream()
                .map(work -> modelMapper.map(work, MCifWorkResponse.class))
                .collect(Collectors.toList());

        return new AppSuccessHandler<>(message, worksResponse);
    }

    @Override
    public AppSuccessHandler<MCifWorkResponse> getWorkById(Long workId) {
        var workResponse = workRepository.findCifWorkById(workId)
                .map(work -> modelMapper.map(work, MCifWorkResponse.class))
                .orElseThrow(() -> throwWorkNotFound(workId));

        var message = new MCifWorkSuccessMessage(AppSuccessCode.GET_DATA);

        return new AppSuccessHandler<>(message, workResponse);
    }

    @Override
    @Transactional
    public AppSuccessHandler<MCifWorkResponse> createWork(MCifWorkRequest workRequest, String role) {
        var cif = cifRepository.findCifById(workRequest.getCifId())
                .orElseThrow(() -> throwCifNotFound(workRequest.getCifId()));

        if (workRepository.getTotalWorkByCifId(cif.getId()) >= MAX_WORK) {
            throw throwMaxWork();
        }

        var work = modelMapper.map(workRequest, MCifWork.class);
        work.create(cif, role);

        var message = new MCifWorkSuccessMessage(AppSuccessCode.CREATE_DATA);
        var workResponse = modelMapper.map(
                workRepository.save(work),
                MCifWorkResponse.class);

        return new AppSuccessHandler<>(message, workResponse);
    }

    @Override
    @Transactional
    public AppSuccessHandler<MCifWorkResponse> updateWork(Long workId, String role, MCifWorkRequest workRequest) {
        var cif = cifRepository.findCifById(workRequest.getCifId())
                .orElseThrow(() -> throwCifNotFound(workRequest.getCifId()));

        if (workRepository.getTotalWorkByCifId(cif.getId()) >= MAX_WORK) {
            throw throwMaxWork();
        }

        var oldWork = workRepository.findCifWorkById(workId)
                .orElseThrow(() -> throwWorkNotFound(workId));

        oldWork.update(modelMapper.map(workRequest, MCifWork.class), cif, role);

        var message = new MCifWorkSuccessMessage(AppSuccessCode.UPDATE_DATA);
        var workResponse = modelMapper.map(
                workRepository.save(oldWork),
                MCifWorkResponse.class);

        return new AppSuccessHandler<>(message, workResponse);
    }

    @Override
    @Transactional
    public AppSuccessHandler<Object> deleteWorkById(Long workId, String role) {
        var workIsExists = workRepository.existCifWorkById(workId);

        if (!workIsExists) {
            throw throwWorkNotFound(workId);
        }

        workRepository.softDelete(workId, role, LocalDateTime.now());

        var message = new MCifWorkSuccessMessage(AppSuccessCode.DELETE_DATA);

        return new AppSuccessHandler<>(message, null);
    }

    private RuntimeException throwEmptyWorks() {
        return new BusinessException(
                new MCifWorkErrorMessage(
                        Map.of(CIF_WORK_IS_EMPTY, new Object()),
                        CIF_WORK_IS_EMPTY));
    }

    private RuntimeException throwMaxWork() {
        return new BusinessException(
                new MCifWorkErrorMessage(
                        Map.of(CIF_WORK_MAX, MAX_WORK),
                        CIF_WORK_MAX));
    }

    private RuntimeException throwWorkNotFound(Long workId) {
        return new BusinessException(
                new MCifWorkErrorMessage(
                        Map.of(CIF_WORK_NOT_FOUND, workId),
                        CIF_WORK_NOT_FOUND));
    }

    private RuntimeException throwCifNotFound(Long cifId) {
        return new BusinessException(
                new MCifErrorMessage(
                        Map.of(CIF_NOT_FOUND, cifId),
                        CIF_NOT_FOUND));
    }

}
