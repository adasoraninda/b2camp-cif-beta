package com.adasoraninda.cifproject.service.impl;

import com.adasoraninda.cifproject.handle.AppSuccessHandler;
import com.adasoraninda.cifproject.handle.exception.BusinessException;
import com.adasoraninda.cifproject.handle.exception.message.MCifErrorMessage;
import com.adasoraninda.cifproject.handle.exception.message.MCifFamilyErrorMessage;
import com.adasoraninda.cifproject.handle.success.code.AppSuccessCode;
import com.adasoraninda.cifproject.handle.success.message.MCifFamilySuccessMessage;
import com.adasoraninda.cifproject.model.entity.MCifFamily;
import com.adasoraninda.cifproject.model.request.MCifFamilyRequest;
import com.adasoraninda.cifproject.model.response.MCifFamilyResponse;
import com.adasoraninda.cifproject.repository.MCifFamilyRepository;
import com.adasoraninda.cifproject.repository.MCifRepository;
import com.adasoraninda.cifproject.service.MCifFamilyService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.adasoraninda.cifproject.handle.exception.code.MCifErrorCode.CIF_NOT_FOUND;
import static com.adasoraninda.cifproject.handle.exception.code.MCifFamilyErrorCode.CIF_FAMILY_IS_EMPTY;
import static com.adasoraninda.cifproject.handle.exception.code.MCifFamilyErrorCode.CIF_FAMILY_NOT_FOUND;

@Service
@AllArgsConstructor
public class MCifFamilyServiceImpl implements MCifFamilyService {

    private final ModelMapper modelMapper;

    private final MCifRepository cifRepository;
    private final MCifFamilyRepository familyRepository;

    @Override
    public AppSuccessHandler<List<MCifFamilyResponse>> getFamilies() {
        var families = familyRepository.findAllCifFamily();

        if (families.isEmpty()) {
            throw throwEmptyFamilies();
        }

        var message = new MCifFamilySuccessMessage(AppSuccessCode.GET_ALL_DATA);
        var familiesResponse = families.stream()
                .map(f -> modelMapper.map(f, MCifFamilyResponse.class))
                .collect(Collectors.toList());

        return new AppSuccessHandler<>(message, familiesResponse);
    }

    @Override
    public AppSuccessHandler<MCifFamilyResponse> getFamilyById(Long familyId) {
        var familyResponse = familyRepository.findCifFamilyById(familyId)
                .map(f -> modelMapper.map(f, MCifFamilyResponse.class))
                .orElseThrow(() -> throwFamilyNotFound(familyId));
        var message = new MCifFamilySuccessMessage(AppSuccessCode.GET_DATA);

        return new AppSuccessHandler<>(message, familyResponse);
    }

    @Override
    @Transactional
    public AppSuccessHandler<MCifFamilyResponse> createFamily(MCifFamilyRequest familyRequest, String role) {
        var cif = cifRepository.findCifById(familyRequest.getCifId())
                .orElseThrow(() -> throwCifNotFound(familyRequest.getCifId()));

        var family = modelMapper.map(familyRequest, MCifFamily.class);
        family.create(cif, role);

        var message = new MCifFamilySuccessMessage(AppSuccessCode.CREATE_DATA);
        var familyResponse = modelMapper.map(
                familyRepository.save(family),
                MCifFamilyResponse.class);

        return new AppSuccessHandler<>(message, familyResponse);
    }

    @Override
    @Transactional
    public AppSuccessHandler<MCifFamilyResponse> updateFamily(Long familyId, String role, MCifFamilyRequest familyRequest) {
        var cif = cifRepository.findCifById(familyRequest.getCifId())
                .orElseThrow(() -> throwCifNotFound(familyRequest.getCifId()));

        var oldFamily = familyRepository.findCifFamilyById(familyId)
                .orElseThrow(() -> throwFamilyNotFound(familyId));

        oldFamily.update(modelMapper.map(familyRequest, MCifFamily.class), cif, role);

        var message = new MCifFamilySuccessMessage(AppSuccessCode.UPDATE_DATA);
        var familyResponse = modelMapper.map(
                familyRepository.save(oldFamily),
                MCifFamilyResponse.class);

        return new AppSuccessHandler<>(message, familyResponse);
    }

    @Override
    @Transactional
    public AppSuccessHandler<Object> deleteFamilyById(Long familyId, String role) {
        var familyIsExists = familyRepository.existCifFamilyById(familyId);

        if (!familyIsExists) {
            throw throwFamilyNotFound(familyId);
        }

        familyRepository.softDelete(familyId, role, LocalDateTime.now());

        var message = new MCifFamilySuccessMessage(AppSuccessCode.DELETE_DATA);

        return new AppSuccessHandler<>(message, null);
    }

    private RuntimeException throwEmptyFamilies() {
        return new BusinessException(
                new MCifFamilyErrorMessage(
                        Map.of(CIF_FAMILY_IS_EMPTY, new Object()),
                        CIF_FAMILY_IS_EMPTY));
    }

    private RuntimeException throwFamilyNotFound(Long familyId) {
        return new BusinessException(
                new MCifFamilyErrorMessage(
                        Map.of(CIF_FAMILY_NOT_FOUND, familyId),
                        CIF_FAMILY_NOT_FOUND));
    }

    private RuntimeException throwCifNotFound(Long cifId) {
        return new BusinessException(
                new MCifErrorMessage(
                        Map.of(CIF_NOT_FOUND, cifId),
                        CIF_NOT_FOUND));
    }

}
