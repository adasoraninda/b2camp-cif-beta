package com.adasoraninda.cifproject.service.impl;

import com.adasoraninda.cifproject.handle.AppSuccessHandler;
import com.adasoraninda.cifproject.handle.exception.BusinessException;
import com.adasoraninda.cifproject.handle.exception.message.MCifAddressErrorMessage;
import com.adasoraninda.cifproject.handle.exception.message.MCifErrorMessage;
import com.adasoraninda.cifproject.handle.success.code.AppSuccessCode;
import com.adasoraninda.cifproject.handle.success.message.MCifAddressSuccessMessage;
import com.adasoraninda.cifproject.model.entity.MCifAddress;
import com.adasoraninda.cifproject.model.request.MCifAddressRequest;
import com.adasoraninda.cifproject.model.response.MCifAddressResponse;
import com.adasoraninda.cifproject.repository.MCifAddressRepository;
import com.adasoraninda.cifproject.repository.MCifRepository;
import com.adasoraninda.cifproject.service.MCifAddressService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.adasoraninda.cifproject.handle.exception.code.MCifAddressErrorCode.CIF_ADDRESS_IS_EMPTY;
import static com.adasoraninda.cifproject.handle.exception.code.MCifAddressErrorCode.CIF_ADDRESS_NOT_FOUND;
import static com.adasoraninda.cifproject.handle.exception.code.MCifErrorCode.CIF_NOT_FOUND;

@Service
@AllArgsConstructor
public class MCifAddressServiceImpl implements MCifAddressService {
    private final ModelMapper modelMapper;

    private final MCifRepository cifRepository;
    private final MCifAddressRepository addressRepository;

    @Override
    public AppSuccessHandler<List<MCifAddressResponse>> getAddresses() {
        var addresses = addressRepository.findAllCifAddress();

        if (addresses.isEmpty()) {
            throw throwAddressEmpty();
        }

        var message = new MCifAddressSuccessMessage(AppSuccessCode.GET_ALL_DATA);
        var addressesResponse = addresses.stream()
                .map(address -> modelMapper.map(address, MCifAddressResponse.class))
                .collect(Collectors.toList());

        return new AppSuccessHandler<>(message, addressesResponse);
    }

    @Override
    public AppSuccessHandler<MCifAddressResponse> getAddressById(Long addressId) {
        var addressResponse = addressRepository.findCifAddressById(addressId)
                .map(address -> modelMapper.map(address, MCifAddressResponse.class))
                .orElseThrow(() -> throwAddressNotFound(addressId));
        var message = new MCifAddressSuccessMessage(AppSuccessCode.GET_DATA);

        return new AppSuccessHandler<>(message, addressResponse);
    }

    @Override
    @Transactional
    public AppSuccessHandler<MCifAddressResponse> createAddress(MCifAddressRequest addressRequest, String role) {
        var cif = cifRepository.findCifById(addressRequest.getCifId())
                .orElseThrow(() -> throwCifNotFound(addressRequest.getCifId()));

        var address = modelMapper.map(addressRequest, MCifAddress.class);
        address.create(cif, role);

        var message = new MCifAddressSuccessMessage(AppSuccessCode.CREATE_DATA);
        var cifResponse = modelMapper.map(
                addressRepository.save(address),
                MCifAddressResponse.class);

        return new AppSuccessHandler<>(message, cifResponse);
    }

    @Override
    @Transactional
    public AppSuccessHandler<MCifAddressResponse> updateAddress(Long addressId, String role, MCifAddressRequest addressRequest) {
        var cif = cifRepository.findCifById(addressRequest.getCifId())
                .orElseThrow(() -> throwCifNotFound(addressRequest.getCifId()));

        var oldAddress = addressRepository.findCifAddressById(addressId)
                .orElseThrow(() -> throwAddressNotFound(addressId));

        oldAddress.update(modelMapper.map(addressRequest, MCifAddress.class), cif, role);

        var message = new MCifAddressSuccessMessage(AppSuccessCode.UPDATE_DATA);
        var addressResponse = modelMapper.map(
                addressRepository.save(oldAddress),
                MCifAddressResponse.class);

        return new AppSuccessHandler<>(message, addressResponse);
    }

    @Override
    @Transactional
    public AppSuccessHandler<Object> deleteAddressById(Long addressId, String role) {
        var addressIsExists = addressRepository.existCifAddressById(addressId);

        if (!addressIsExists) {
            throw throwCifNotFound(addressId);
        }

        addressRepository.softDelete(addressId, role, LocalDateTime.now());

        var message = new MCifAddressSuccessMessage(AppSuccessCode.DELETE_DATA);

        return new AppSuccessHandler<>(message, null);
    }

    private RuntimeException throwAddressEmpty() {
        return new BusinessException(
                new MCifAddressErrorMessage(
                        Map.of(CIF_ADDRESS_IS_EMPTY, new Object()),
                        CIF_ADDRESS_IS_EMPTY));
    }

    private RuntimeException throwAddressNotFound(Long addressId) {
        return new BusinessException(
                new MCifAddressErrorMessage(
                        Map.of(CIF_ADDRESS_NOT_FOUND, addressId),
                        CIF_ADDRESS_NOT_FOUND));
    }

    private RuntimeException throwCifNotFound(Long cifId) {
        return new BusinessException(
                new MCifErrorMessage(
                        Map.of(CIF_NOT_FOUND, cifId),
                        CIF_NOT_FOUND));
    }

}
