package com.adasoraninda.cifproject.service;

import com.adasoraninda.cifproject.handle.AppSuccessHandler;
import com.adasoraninda.cifproject.model.request.MCifRequest;
import com.adasoraninda.cifproject.model.response.MCifResponse;

import java.util.List;

public interface MCifService {

    AppSuccessHandler<List<MCifResponse>> getListCif();

    AppSuccessHandler<MCifResponse> getCifById(Long cifId);

    AppSuccessHandler<MCifResponse> createCif(MCifRequest cifRequest, String role);

    AppSuccessHandler<MCifResponse> updateCif(Long cifId, String role, MCifRequest cifRequest);

    AppSuccessHandler<Object> deleteCifById(Long cifId, String role);

}
