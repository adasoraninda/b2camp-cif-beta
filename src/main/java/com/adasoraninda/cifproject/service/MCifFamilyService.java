package com.adasoraninda.cifproject.service;

import com.adasoraninda.cifproject.handle.AppSuccessHandler;
import com.adasoraninda.cifproject.model.request.MCifFamilyRequest;
import com.adasoraninda.cifproject.model.response.MCifFamilyResponse;

import java.util.List;

public interface MCifFamilyService {

    AppSuccessHandler<List<MCifFamilyResponse>> getFamilies();

    AppSuccessHandler<MCifFamilyResponse> getFamilyById(Long familyId);

    AppSuccessHandler<MCifFamilyResponse> createFamily(MCifFamilyRequest familyRequest, String role);

    AppSuccessHandler<MCifFamilyResponse> updateFamily(Long familyId, String role, MCifFamilyRequest familyRequest);

    AppSuccessHandler<Object> deleteFamilyById(Long familyId, String role);

}
