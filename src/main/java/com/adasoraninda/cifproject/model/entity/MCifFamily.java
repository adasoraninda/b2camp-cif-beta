package com.adasoraninda.cifproject.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "m_cif_family")
@EqualsAndHashCode(callSuper = true)
public class MCifFamily extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String type;

    @ManyToOne
    @JoinColumn(name = "cif_id", referencedColumnName = "id", nullable = false)
    private MCif cif;

    public void create(MCif cif, String role) {
        super.create(role);
        this.cif = cif;
    }

    public void update(MCifFamily newFamily, MCif cif, String role) {
        super.update(role);
        this.name = newFamily.getName();
        this.type = newFamily.getType();
        this.cif = cif;
    }
}
