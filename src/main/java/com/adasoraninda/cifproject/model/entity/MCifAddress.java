package com.adasoraninda.cifproject.model.entity;

import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "m_cif_address")
@EqualsAndHashCode(callSuper = true)
public class MCifAddress extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String address;

    @ManyToOne
    @JoinColumn(name = "cif_id", referencedColumnName = "id", nullable = false)
    private MCif cif;

    public void create(MCif cif, String role) {
        super.create(role);
        this.cif = cif;
    }

    public void update(MCifAddress newAddress, MCif cif, String role) {
        super.update(role);
        this.name = newAddress.getName();
        this.address = newAddress.getAddress();
        this.cif = cif;
    }
}
