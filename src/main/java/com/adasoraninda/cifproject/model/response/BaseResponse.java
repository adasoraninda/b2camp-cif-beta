package com.adasoraninda.cifproject.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseResponse<T> {
    private String code;
    private String message;
    private T data;

    public static <T> BaseResponse<T> success(String message, T data) {
        return new BaseResponse<>(
                ResponseType.SUCCESS.name(),
                message,
                data);
    }

    public static BaseResponse<Object> error(String code, String message) {
        return new BaseResponse<>(
                code,
                message,
                null
        );
    }

    public static BaseResponse<Object> error(String message) {
        return new BaseResponse<>(
                ResponseType.ERROR.name(),
                message,
                null
        );
    }

    private enum ResponseType {
        SUCCESS, ERROR
    }

}
