package com.adasoraninda.cifproject.model.response;

import lombok.Data;

@Data
public class MCifAddressResponse {
    private Long id;
    private String name;
    private String address;
    private MCifResponse cif;
}
